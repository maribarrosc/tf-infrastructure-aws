terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket  = "tf-bucket"
    key     = "env/terraform.tfstate"
    region = "us-east-1"
  }
} 
