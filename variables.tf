variable "environment" {
    default = {
        default = "default"
        stg     = "stage"
        prod    = "production"
    }
}

locals {
    env = lookup(var.environment, terraform.workspace)
}

output "env-current" {
    value = local.env
}

# "${var.name}-${var.environment}"