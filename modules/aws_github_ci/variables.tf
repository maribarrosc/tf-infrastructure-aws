# Github
variable "namespace" {
  description   = "AWS resource namespace/prefix"
}

variable "github_owner" {
  description = "GitHub repository owner"
}

variable "github_repository" {
  description = "GitHub repository name"
}

variable "github_oauth_token" {
  description = "GitHub OAuth token for repository access"
}

# CodeBuild

variable "codebuild_compute_type" {
  description = "Compute resources used by the build"
  default     = "BUILD_GENERAL1_SMALL"
}

variable "codebuild_image" {
  description = "Base image for provisioning"
  default     = "aws/codebuild/ubuntu-base:14.04"
}

variable "codebuild_buildspec" {
  description = "Build specification file location"
  default     = ""
}

variable "codebuild_privileged_mode" {
  description = "Enables running the Docker daemon inside a Docker container"
  default     = "false"
}

variable "codebuild_bucket" {
  description = "S3 bucket to store status badge and artifacts"
  default     = ""
}

variable "codebuild_environment_variables" {
  description = "Environment variables to be used for build"
  default     = []
  type        = "list"
}

variable "codebuild_badge_enabled" {
  description = "Generates a publicly-accessible URL for the projects build badge"
  default     = "true"
}