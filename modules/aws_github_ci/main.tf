# Data: General
data "aws_region"  "_" {}

# Data: GitHub
data "template_file" "codebuild_source_location" {
    template = "https://github.com/$${owner}/$${respository}.git"

    vars {
        owner      = var.github_owner
        repository = var.github_repository
    }
}

# Data: IAM
data "template_file" "codebuild_iam_policy" {
  template = "${file("${path.module}/iam/policies/codebuild.json")}"

  vars {
    bucket = "${aws_s3_bucket._.bucket}"
  }
}

# Resources: IAM
resource "aws_iam_role" "codebuild" {
  name = "${var.namespace}-codebuild"
  path = "/${var.namespace}/codebuild/"

  assume_role_policy = "${file("${path.module}/iam/policies/assume-role/codebuild.json")}"
}

resource "aws_iam_policy" "codebuild" {
  name = "${var.namespace}-codebuild"
  path = "/${var.namespace}/codebuild/"

  policy = data.template_file.codebuild_iam_policy.rendered
}

resource "aws_iam_policy_attachment" "codebuild" {
  name = "${var.namespace}-codebuild"

  policy_arn = aws_iam_policy.codebuild.arn
  roles      = [aws_iam_role.codebuild.name]
}

# Resources: S3
resource "aws_s3_bucket" "_" {
  bucket = "${coalesce(var.codebuild_bucket, var.namespace)}"
  acl    = "private"
}

# Resources: CodeBuild
resource "aws_codebuild_project" "_" {
  name = var.github_repository

  build_timeout = "5"
  service_role  = aws_iam_role.codebuild.arn
  badge_enabled = var.codebuild_badge_enabled

  source {
    type      = "GITHUB"
    location  = data.template_file.codebuild_source_location.rendered
    buildspec = var.codebuild_buildspec

    auth {
      type     = "OAUTH"
      resource = var.github_oauth_token
    }
  }

  environment {
    compute_type         = var.codebuild_compute_type
    type                 = "LINUX_CONTAINER"
    image                = var.codebuild_image
    privileged_mode      = var.codebuild_privileged_mode
    environment_variable = var.codebuild_environment_variables
  }

  artifacts {
    type           = "S3"
    location       = aws_s3_bucket._.bucket
    name           = var.github_repository
    namespace_type = "BUILD_ID"
    packaging      = "ZIP"
  }
}

resource "aws_codebuild_webhook" "_" {
  project_name = aws_codebuild_project._.name
}
