output "codebuild_service_role_name" {
  value = "${aws_iam_role.codebuild.name}"
}

output "codebuild_service_role_name" {
  value = aws_iam_role.codebuild.name
}

output "codebuild_bucket" {
  value = aws_s3_bucket._.bucket
}

output "codebuild_badge_url" {
  value = "${
    var.codebuild_badge_enabled == "true"
      ? aws_codebuild_project._.*.badge_url[0]
      : ""
  }"
}

output "codebuild_url" {
  value = "https://console.aws.amazon.com/codebuild/home?region=${
    data.aws_region._.name
  }#/projects/${
    var.github_repository
  }/view"
}
