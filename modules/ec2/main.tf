resource "aws_key_pair" "tf_ssh" {
  key_name   = "tf_ssh"
  public_key = ""

}

resource "aws_instance" "kibana" {
  ami                           = var.ubuntu_ami
  associate_public_ip_address   = true
  instance_type                 = var.instance_type
  key_name                      = "tf_ssh"
  vpc_security_group_ids        = [aws_security_group.this.id]
  subnet_id                     = var.subnet_id

  tags  = {
      Name = "kibana-v2"
  }

  depends_on = [aws_security_group.this]

}