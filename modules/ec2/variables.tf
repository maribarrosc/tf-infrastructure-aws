
variable "instance_type" {
  default     = "t2.small"
  description = "Tipo de instância"
}

variable "ubuntu_ami" {
    default     = "ami-04b9e92b5572fa0d1"
}

variable "subnet_id" {
    description = "Subnet to launch bastion"
    type        = string
    default     = "subnet-0cde0dbf3b324d50f"
}
variable "vpc_id" {
    description = "Vpc id to setup bastion"
    type        = string
    default     = "vpc-049dc04f06eac73ca"
}

data "aws_vpc" "selected" {
    id  = var.vpc_id
}

variable "key_name" {
  description = "The key name to use for the instance"
  type        = string
  default     = ""
}
