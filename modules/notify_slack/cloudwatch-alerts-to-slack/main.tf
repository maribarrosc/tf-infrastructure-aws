provider "aws" {
    region = var.region
}

resource "aws_kms_key" "main" {
    description     = "KMS key for notify-slack test"
}

resource "aws_kms_ciphertext" "slack_url" {
    plaintext = "https://hooks.slack.com/services/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"
    key_id    = aws_kms_key.main.arn
}

module "notify_slack" {
  source              = "../../notify_slack"
  sns_topic_name      = "notify-topic-slacks"
  slack_webhook_url   = aws_kms_ciphertext.slack_url.ciphertext_blob
  slack_channel       = "infra-alarms"
  slack_username      = "reporter"
  kms_key_arn         = aws_kms_key.main.arn

  lambda_description  = "Lambda function which sends notifications to Slack"
  log_events          = true

  tags = {
    Name = "cloudwatch-alerts-to-slack"
  }
}

resource "aws_cloudwatch_metric_alarm" "LambdaDuration" {
  alarm_name          = "NotifySlackDuration"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Duration"
  namespace           = "AWS/Lambda"
  period              = "60"
  statistic           = "Average"
  threshold           = "5000"
  alarm_description   = "Duration of notifying slack exceeds threshold"

  alarm_actions = [module.notify_slack.main_slack_topic_arn]

  dimensions = {
    FunctionName = module.notify_slack.notify_slack_lambda_function_name
  }
}