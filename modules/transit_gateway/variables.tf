variable "vpc_id" {
    default     = "vpc-0f4c9ff09b9b55388"
}
variable "public_subnet1" {
    default     = "subnet-08f7b1610cc826221"
}
variable "public_subnet2" {
    default     = "subnet-041acad973b042123"
}