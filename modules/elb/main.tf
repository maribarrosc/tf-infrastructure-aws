provider "aws" {
  region = var.region
}

data "aws_elb_service_account" "main" {}
data "aws_availability_zones" "all" {}
data "aws_subnet_ids" "main" {
  vpc_id = var.vpc_id

  tags = {
    Tier = var.internal ? "Private" : "Public"
  }
}

data "aws_subnet" "main" {
  count = length(data.aws_subnet_ids.main.ids)
  id    = tolist(data.aws_subnet_ids.main.ids)[count.index]
}

resource "aws_s3_bucket" "elb_logs" {
  bucket = "lb-logs-tf"
  acl    = var.acl_private

  policy = <<POLICY
{
  "Id": "Policy",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::lb-logs-tf/AWSLogs/*",
      "Principal": "*"
    }
  ]
}
POLICY

  tags = {
    Product = var.name
  }
}

resource "aws_elb" "main" {
  name                          = var.internal ? "${var.name}-internal-lb" : "${var.name}-external-lb"
  # availability_zones            = ["us-east-1a"]
  subnets                       = ["subnet-0cde0dbf3b324d50f"]
  # subnets                       = tolist(data.aws_subnet_ids.main.ids)
  security_groups               = [aws_security_group.main.id]
  # cross_zone_load_balancing     = var.cross_zone_load_balancing
  
  access_logs {
    bucket   = aws_s3_bucket.elb_logs.bucket
    interval = 5
  }

  listener {
    instance_port     = var.instance_port
    instance_protocol = "http"
    lb_port           = var.lb_port
    lb_protocol       = "http"
  }

  listener {
    instance_port    = var.instance_port
    instance_protocol = "http"
    lb_port           = var.lb_port_pv
    lb_protocol       = "https"
    ssl_certificate_id = var.ssl_certificate_id

  }
  
  tags  = {
      Product = var.name
    }
}
 