data "aws_vpc" "selected" {
    id = var.vpc_id
}

resource "aws_security_group" "main" {
  name        = var.internal ? "${var.name}-internal-lb-sg" : "${var.name}-external-lb-sg"
  vpc_id      = data.aws_vpc.selected.id

  # TODO main should be more restricted
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "internet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  lifecycle {
    ignore_changes        = [ingress]
    create_before_destroy = true
  }

  tags = {
    Name    = var.internal ? "${var.name}-internal-lb-sg" : "${var.name}-external-lb-sg"
    Product = var.name
  }
}

resource "aws_security_group_rule" "public" {
  count             = var.internal ? 0 : 1
  security_group_id = aws_security_group.main.id
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow https"
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
}
