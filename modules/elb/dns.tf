data "aws_elb_hosted_zone_id" "main" {}

resource "aws_route53_record" "main" {
    zone_id     = var.zone_id
    name        = var.domain
    type        = "A"
    alias {
        name                    = aws_elb.main.dns_name
        zone_id                 = aws_elb.main.zone_id
        evaluate_target_health  = true
    }
}
