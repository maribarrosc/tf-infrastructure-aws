variable "internal" {
  description = "Set if load balancer is internal or external"
  default     = false
  type        = bool
}

variable "config" {
  type = map
}

variable "name" {
  description = "Application name"
  type        = string
}

variable "domain" {
  description = "Domain/Url of the application"
  type        = string
  default     = ""
}

variable "vpc_id" {
  description = "VPC Id"
  type        = string
  default     = "vpc-049dc04f06eac73ca"
}

variable "zone_id" {
  description = "Dns zone id"
  type        = string
  default     = "Z19MSAJASZQZQI"
}

variable "region" {
  default     = "us-east-1"
  description = "Aws region"
  type        = string
}

variable "cross_zone_load_balancing" {
  default = true
  type    = bool
}

variable "instance_port" {
  default = 8000
  type    = number
}

variable "lb_port" {
  type  = number
  default = 80
}

variable "lb_port_pv" {
  type  = number
  default = 443
}

variable "ssl_certificate_id" {
  default = "arn:aws:acm:us-east-1:384867261623:certificate/8b8ba506-4681-4f9d-a59a-dc02b5440999"
  type    = string
}

variable "acl_private" {
  default = "private"
  type    = string
}