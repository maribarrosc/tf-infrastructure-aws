resource "aws_subnet" "private" {
  availability_zone = var.availability_zones[count.index]
  count             = local.count
  cidr_block        = "${var.subnet_prefix}.${var.start_ip+count.index}.0/24"
  vpc_id            = data.aws_vpc.selected.id
  
  tags = {
    Name    = "Private-${local.env}-${var.availability_zones[count.index]}"
    Tier    = "Private"
  }
}

resource "aws_route_table_association" "main" {
  count           = local.count
  route_table_id  = data.aws_vpc.selected.main_route_table_id
  subnet_id       = aws_subnet.private[count.index].id
}
