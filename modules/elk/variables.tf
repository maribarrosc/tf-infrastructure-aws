variable "domain" {
  default = "zeenow"
}

variable "es_version" {
    default     = "7.1"
    description = "elasticsearch version"
}

variable "es_count" {
    default     = "2"
    description = "elasticsearch count"
}

variable "es_dedicated_master_count" {
    default = "3"
}

variable "vpc" {
    default = "vpc-049dc04f06eac73ca"
}