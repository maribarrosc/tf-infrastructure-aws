data "aws_vpc" "main" {
  tags = {
    Name = var.vpc
  }
}

data "aws_subnet_ids" "main" {
  vpc_id = data.aws_vpc.main.id
  tags = {
    Tier = "private"
  }
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

resource "aws_cloudwatch_log_group" "zeenow" {
  name = "zeenow-elastic"
}

resource "aws_cloudwatch_log_resource_policy" "zeenow" {
  policy_name = "zeenow"

  policy_document = <<CONFIG
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Action": [
        "logs:PutLogEvents",
        "logs:PutLogEventsBatch",
        "logs:CreateLogStream"
      ],
      "Resource": "arn:aws:logs:*"
    }
  ]
}
CONFIG
}

resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
}

resource "aws_elasticsearch_domain" "es" {
  domain_name                = var.domain
  elasticsearch_version      = var.es_version

  cluster_config {
    instance_type            = "m3.medium.elasticsearch"
    instance_count           = var.es_count
    dedicated_master_enabled = "true"
    dedicated_master_type    = "m3.medium.elasticsearch"
    dedicated_master_count   = var.es_dedicated_master_count
  }

  log_publishing_options {
    cloudwatch_log_group_arn = aws_cloudwatch_log_group.zeenow.arn
    log_type                 = "INDEX_SLOW_LOGS"
    enabled                  = "true"
  }

  ebs_options {
    ebs_enabled = "true"
    volume_size = "15"
  }

  encrypt_at_rest {
    enabled = "false"
  }

  node_to_node_encryption {
    enabled = true
  }

 ## ADICIONAR IP EXTERNO NA POLITICA
  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": "*",
            "Effect": "Allow",
            "Resource": "arn:aws:es:data.aws_region.current.name:data.aws_caller_identity.current.account_id:domain/var.domain/*",
            "Condition": {
                "IpAddress": {
                    "aws:SourceIp": [
                        "189.112.55.77"
                    ]
                }
            }
        }
    ]
}
CONFIG

  snapshot_options {
    automated_snapshot_start_hour = 0
  }

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  tags = {
    Domain = var.domain
  }

  depends_on = [aws_iam_service_linked_role.es]
}
