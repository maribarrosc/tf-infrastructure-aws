# locals {
#   name  = (var.name, "-", "-")
# }

locals {
  count = length(var.availability_zones)
  env = lookup(var.environments, terraform.workspace)
}

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr

  tags = {
      Name    = upper("VPC_${local.env}")
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
    tags = {
      Name    = upper("IGW_${local.env}")
  }
}

resource "aws_subnet" "public" {
  availability_zone = var.availability_zones[count.index]
  count             = local.count
  cidr_block        = "${var.subnet_prefix}.${var.start_ip+count.index}.0/24"
  vpc_id            = aws_vpc.main.id
  
  tags = {
    Name = "Public-${local.env}-${var.availability_zones[count.index]}"
    Tier = "Public"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
      Name = upper("RT_PUBLIC_${local.env}")
  }
}

resource "aws_route_table_association" "main" {
  count           = local.count
  subnet_id       = aws_subnet.public[count.index].id
  route_table_id  = aws_route_table.public.id
}

resource "aws_eip" "nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.main]
  tags = {
    Name        = "${local.env}-eip"
  }
}

resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = element(aws_subnet.public.*.id, 0)
  depends_on    = [aws_internet_gateway.main]

  tags = {
    Name        = "${local.env}-nat-gateway"
  }
}

resource "aws_default_route_table" "main" {
  default_route_table_id = aws_vpc.main.default_route_table_id

  route {
    cidr_block      = "0.0.0.0/0"
    nat_gateway_id  = aws_nat_gateway.main.id
  }

  tags = {
    Name    = upper("RT_PRIVATE_${local.env}")
  }
}