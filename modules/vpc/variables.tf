variable "vpc_cidr" {
  default  = "173.31.0.0/16"

}

# variable "transit_gateway" {}
# zeenow-admin-user


variable "availability_zones" {
  description = "AWS AVailability Zones"
}

variable "start_ip" {
  description = "Start ip of public subnets"
}

variable "subnet_prefix" {
  description = "Prefix for setup of subnets"
}

variable "environments" {
    default = {
        default = "default"
        stg     = "stage"
        prod    = "production"
    }
}