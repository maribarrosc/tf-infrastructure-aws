variable "root_domain_name" {
    description = "Main domain of the application"
    type        = string
}

variable "config" {
  type = map
}