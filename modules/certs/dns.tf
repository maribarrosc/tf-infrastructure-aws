resource "aws_route53_zone" "main" {
    name   = var.domain
}

resource "aws_route53_record" "main" {
    allow_overwrite = true
    name            = var.domain
    ttl             = "30"
    type            = "NS"
    zone_id         = aws_route53_zone.main.zone_id

    records = [
        "${aws_route53_zone.main.name_servers.0}",
        "${aws_route53_zone.main.name_servers.1}",
        "${aws_route53_zone.private.name_servers.2}",
        "${aws_route53_zone.private.name_servers.3}",
    ]
}

resource "aws_route53_zone" "private" {
    name = var.domain

    vpc {
        vpc_id = var.vpc
    }
}
