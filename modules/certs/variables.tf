variable "domain" {
    description = "Primary certificate domain name"
    type        = string
    default     = "www.zeedog.com.br"
}

variable "subject_alternative_names" {
    default     = []
    type        = list(string)
    description = "Subject alternative domain names"
}

# variable "name" {
#     description = "App name"
#     type        = string
# }

variable "type" {
    description = "API or Static"
    type        = string
    default     = "api"
}

variable "hosted_zone_id" {
    description = "Route 53 Zone ID for DNS validation records"
    type        = string
}

variable "validation_record_ttl" {
    default     = 60
    type        = number
    description = "Route 53 time-to-live for validation records"
}

variable "vpc" {
    default = "vpc-049dc04f06eac73ca"
    type   = string
}

variable "allow_validation_record_overwrite" {
    default     = true
    type        = bool
    description = "Allow Route 53 record creation to overwrite existing records"
}

variable "tags" {
    default     = {}
    type        = map(string)
    description = "Extra tags to attach to the ACM certificate"
}