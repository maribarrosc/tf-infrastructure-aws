resource "aws_acm_certificate" "default" {
    domain_name               = var.domain
    validation_method         = "DNS"
    subject_alternative_names = var.subject_alternative_names

    tags = merge(
        {
            Name = var.domain
        }, 
        var.tags,
        )

    lifecycle {
        create_before_destroy = true

    }
}

resource "aws_route53_record" "validation" {
    count           = length(var.subject_alternative_names) + 1
    name            = aws_acm_certificate.default.domain_validation_options.0.resource_record_name
    zone_id         = var.hosted_zone_id
    type            = "CNAME"
    records         = [aws_acm_certificate.default.domain_validation_options.0.resource_record_value]
    ttl             = var.validation_record_ttl
    allow_overwrite = var.allow_validation_record_overwrite

}
# aws_acm_certificate.default.domain_validation_options.0.resource_record_type

resource "aws_acm_certificate_validation" "default" {
    certificate_arn         = aws_acm_certificate.default.arn
    validation_record_fqdns = aws_route53_record.validation.*.fqdn
    timeouts {
        create = "60m"
    }
}

