variable "vpc_id" {
    description = "Vpc id to setup bastion"
    type        = string
    default     = "vpc-049dc04f06eac73ca"
}

variable "config" {
  type = map
}

variable "region" {
    default     = "us-east-1"
    description = "Aws region"
    type        = string
}

variable "ec2_role" {
    type    = string
    default = "arn:aws:iam::384867261623:instance-profile/aws-opsworks-ec2-role"
}

variable "service_role" {
    type    = string
    default = "arn:aws:iam::384867261623:role/aws-opsworks-service-role"
}

variable "tf_ssh" {
    default = ""
}

variable "sg_ops" {
    default = "subnet-05501bf92fcb57016"
    type    = string
}

variable "db_user" {
    default = "shows"
    type    = string
}

variable "db_password" {
    default = "show"
    type    = string
}

variable "db_arn" {
    default    = "arn:aws:rds:us-east-1:384867261623:db:tf-postgres"
}

variable "db_name" {
    default = "tf_pg"
}

variable "db_source_type" {
    default = "RdsDbInstance"
}

variable "cor" {
    default = "rgb(186, 65, 50)"
}

variable "system_os" {
    default = "Ubuntu 16.04 LTS"
}

variable "key_name" {
    default = "tf_ssh"
}

variable "url_git" {
    default = "git@github.com:ajgon/opsworks_ruby.git"
}

variable "branch" {
    default = "master"
}
variable "security_id" {
    default = ["sg-0642f7afb9d6e02a0", "sg-007e5b33358027ea3"]
    type    = list
}

variable "app_git_url" {
    default = "git@github.com:ZeeDog/tf-api.git"
}

variable "lb_api" {
  type    = string
  default = ""
}