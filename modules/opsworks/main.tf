provider "aws" {
  region = var.region
}

# data "aws_lb" "main" {
#   name = var.lb_name
# }

resource "aws_opsworks_stack" "main" {
  name                          = "staging_zeenow_api"
  region                        = var.region
  service_role_arn              = var.service_role
  default_instance_profile_arn  = var.ec2_role
  color                         = var.cor
  vpc_id                        = var.vpc_id
  default_subnet_id             = var.sg_ops
  default_os                    = var.system_os 
  use_opsworks_security_groups  = false
  use_custom_cookbooks          = true
  configuration_manager_version = "12"
  default_ssh_key_name          = var.key_name
 
  custom_cookbooks_source {
    type      = "git"
    url       = var.url_git
    ssh_key   = var.zeenow_ssh
    revision  = var.branch
  }

  custom_json = <<EOT
{
 "foobar": {
    "version": "1.0.0"
  }
}
EOT
}

resource "aws_opsworks_rds_db_instance" "zeenow-postgres" {
  stack_id            = aws_opsworks_stack.main.id
  rds_db_instance_arn = "arn:aws:rds:us-east-1:384867261623:db:zeenow-postgres"
  db_user             = var.db_user
  db_password         = var.db_password
}

resource "aws_opsworks_custom_layer" "custlayer" {
  name                         = "zeenow-api"
  short_name                   = "zn-api"
  stack_id                     = aws_opsworks_stack.main.id
  instance_shutdown_timeout    = 120
  auto_healing                 = true
  custom_instance_profile_arn  = var.ec2_role
  custom_security_group_ids    = var.security_id
  auto_assign_elastic_ips      = true
  auto_assign_public_ips       = true
  install_updates_on_boot      = true
  elastic_load_balancer        = "teste"
  drain_elb_on_shutdown        = true

  custom_configure_recipes     = ["opsworks_ruby::setup"]
  custom_deploy_recipes        = ["opsworks_ruby::configure"]
  custom_setup_recipes         = ["opsworks_ruby::deploy"]
  custom_shutdown_recipes      = ["opsworks_ruby::undeploy"]
  custom_undeploy_recipes      = ["opsworks_ruby::shutdown"] 
}


resource "aws_opsworks_instance" "api-instance" {
  stack_id        = aws_opsworks_stack.main.id
  layer_ids       = [
    aws_opsworks_custom_layer.custlayer.id,
  ]
  root_device_type = "ebs" 
  instance_type    = "t3.medium"
  os               = "Ubuntu 16.04 LTS"
  state            = "stopped"
}

resource "aws_cloudwatch_metric_alarm" "api-redis-cls01" {
  alarm_name          = "api-redis-cls01"
  alarm_description   = "This metric monitors ec2 cpu utilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  threshold           = 0
  period              = 60
  namespace           = "AWS/EC2"
  metric_name         = "CPUUtilization"
  statistic           = "Average"
  actions_enabled     = true

  alarm_actions = ["arn:aws:sns:us-east-1:384867261623:notify-topic-slacks"]
  dimensions = {
    CacheClusterId="notify-topic-slacks"
  }
}

resource "aws_opsworks_application" "app_api_prod" {
  name        = "api prod application2"
  short_name  = "api_prod"
  stack_id    = aws_opsworks_stack.main.id
  type        = "other"

  app_source {
    type     = "git"
    revision =  var.branch
    url      = var.app_git_url
    ssh_key  = var.zeenow_ssh
  }

  data_source_type            = var.db_source_type
  data_source_database_name   = var.db_name
  data_source_arn             = var.db_arn
}

