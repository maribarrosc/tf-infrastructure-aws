variable "project" {
    default     = "Unknown"
    type        = string
    description = "Name of project this VPC is meant to house"
}

variable "environment" {
    default     = "Unknown"
    type        = string
    description = "Name of environment this VPC is targeting"
}

variable "instance_type" {
    default     = "db.t3.medium"
    type        = string
    description = "Instance type for database instance"
} 

variable "engine_version" {
    default     = "11.5"
    type        = string
    description = "Database engine version"
}

variable "allocated_storage" {
    type        = number
    default     = 100
    description = "Storage allocated to database instance"
}

variable "storage_type" {
    default     = "gp2"
    type        = string
    description = "Type of underlying storage for database"
}

variable "database_identifier" {
    type        = string
    description = "Identifier for RDS instance"
    default     = "tf-postgres"
}

variable "database_name" {
    default     = "tf_pg"
    type        = string
    description = "Name of database inside storage engine"
}

variable "backup_retention_period" {
    default     = 30
    type        = number
    description = "Number of days to keep database backups"
}

variable "multi_availability_zone" {
    default     = true
    type        = bool
    description = "Flag to enable hot standby in another availability zone"
}

variable "monitoring_interval" {
    default     = 5
    type        = number
    description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected"
}

variable "skip_final_snapshot" {
    default     = true
    type        = bool
    description = "Flag to enable or disable a snapshot if the database instance is terminated"
}

variable "final_snapshot_identifier" {
    default     = "terraform-aws-postgresql-rds-snapshot"
    type        = string
    description = "Identifier for final snapshot if skip_final_snapshot is set to false"
}

variable "engine" {
    type    = string
    default =  "postgres"
}

variable "deletion_protection" {
    default     = true
    type        = bool
    description = "Flag to protect the database instance from deletion"
}

variable "cloudwatch_logs_exports" {
    default     = ["postgresql", "upgrade"]
    type        = list
    description = "List of logs to publish to CloudWatch Logs"
}

variable "alarm_cpu_threshold" {
    default     = 75
    type        = number
    description = "CPU alarm threshold as a percentage"
}

variable "alarm_disk_queue_threshold" {
    default     = 10
    type        = number
    description = "Disk queue alarm threshold"
}

variable "alarm_free_disk_threshold" {
    # 5GB
    default     = 5000000000
    type        = number
    description = "Free disk alarm threshold in bytes"
}

variable "alarm_free_memory_threshold" {
    # 128MB
    default     = 128000000
    type        = number
    description = "Free memory alarm threshold in bytes"
}

variable "alarm_cpu_credit_balance_threshold" {
    default     = 30
    type        = number
    description = "CPU credit balance threshold (only for db.t* instance types)"
}

variable "alarm_actions" {
    type        = list
    description = "List of ARNs to be notified via CloudWatch when alarm enters ALARM state"
    default     = ["arn:aws:sns:us-east-1:384867261623:notify-topic-slacks"]

}

variable "ok_actions" {
    type        = list
    description = "List of ARNs to be notified via CloudWatch when alarm enters OK state"
    default     = ["arn:aws:sns:us-east-1:384867261623:notify-topic-slacks"]
}

variable "insufficient_data_actions" {
    type        = list
    description = "List of ARNs to be notified via CloudWatch when alarm enters INSUFFICIENT_DATA state"
    default     = ["arn:aws:sns:us-east-1:384867261623:notify-topic-slacks"]

}

variable "name" {
    description = "Name of application"
    type        = string
}

variable "vpc_id" {
    type        = string
    description = "ID of VPC meant to house database"
    default     = "vpc-049dc04f06eac73ca"
}

variable "postgresql_cidr" {
    description = "Cidr of the subnet where the bastion host is running"
    default     = ""
}

variable "tags" {
    default     = {}
    type        = map(string)
    description = "Extra tags to attach to the RDS resources"
}

variable "username" {
    default   = "ud14ln17k28je"
    type      = string
}

variable "password" {
    default  = "p5ef15fa48d1afebfbdc9570f54b56b5dc5c0e6b3a02c576f861899bf4edc4d89"
    type     = string
}

variable "region" {
    default     = "us-east-1"
}
