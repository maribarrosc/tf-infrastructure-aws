data "aws_vpc" "selected" {
    id          = var.vpc_id
}

data "aws_subnet_ids" "private" {
  vpc_id        = var.vpc_id

  tags = {
    Tier = "Private"
  }
}

data "aws_subnet" "private" {
  count = length(data.aws_subnet_ids.private.ids)
  id    = tolist(data.aws_subnet_ids.private.ids)[count.index]
}

resource "aws_security_group" "main" {
    name        = "${var.name}-db-sg"
    vpc_id      = data.aws_vpc.selected.id

    egress {
      cidr_blocks = ["0.0.0.0/0"]
      description = "internet"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
    }

    lifecycle {
      create_before_destroy = true
    }

    tags = {
      Name = "${var.name}-db-sg"
    }
}

resource "aws_security_group_rule" "main" {
  cidr_blocks       = data.aws_subnet.private.*.cidr_block
  description       = "postgres-port"
  from_port         = 5432
  to_port           = 5432
  type              = "ingress"
  protocol          = "tcp"
  security_group_id = aws_security_group.main.id
}

resource "aws_security_group_rule" "postgresql" {
  count             = var.postgresql_cidr != "" ? 1 : 0
  cidr_blocks       = [var.postgresql_cidr]
  description       = "postgres-access"
  from_port         = 5432
  to_port           = 5432
  type              = "ingress"
  protocol          = "tcp"
  security_group_id = aws_security_group.main.id
}
