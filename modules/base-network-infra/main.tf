provider "aws" {
  version = "~> 2.46"
  region  = var.region
}

terraform {
  backend "s3"{
    bucket  = "tf-bucket"
    key     = "env/terraform.tfstate"
    region  = "us-east-1"
  }
}

locals {
  env = lookup(var.environments, terraform.workspace, var.name)
}
# locals {
#   name_dash   = "${var.name}-${var.environment}"
# }

module "vpc" {
  source              = "../modules/vpc"
  availability_zones  = var.availability_zones
  name                = local.env
  start_ip            = var.public_subnet_start_ip
  subnet_prefix       = var.subnet_prefix
  vpc_cidr            = var.vpc_cidr
}

module "private-subnets" {
  source              = "../modules/private-subnets"
  availability_zones  = var.availability_zones
  name                = local.env
  start_ip            = var.private_subnet_start_ip
  subnet_prefix       = var.subnet_prefix
  vpc_id              = module.vpc.vpc_id
}
