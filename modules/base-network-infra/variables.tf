variable "availability_zones" {
  type          = list
  default       = ["us-east-1a", "us-east-1b", "us-east-1c"]
  description   = "AWS Availability zones"
}

# variable "environment" {
#   type        = string
#   description = "App environ - stg. prd."
#   default     = "stg"
# }

# variable "name" {
#   type        = string
#   description = "Product Name"
# }

variable "private_subnet_start_ip" {
  description = "subnet_prefix + start_ip (10.0.30)"
  default     = "30"
  type        = string
}

variable "public_subnet_start_ip" {
  description = "subnet_prefix + start_ip (10.0.20)"
  default     = "20"
  type        = string
}

variable "region" {
  default     = "us-east-1"
  description = "Aws region"
}

variable "subnet_prefix" {
  description = "Prefix for setup of subnets"
  default     = "10.0"
  type        = string
}

variable "vpc_cidr" {
  description = "CIDR of the VPC"
  default     = "10.0.0.0/16"
  type        = string
}
