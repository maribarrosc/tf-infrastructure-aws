resource "aws_s3_bucket" "bucket" {
    bucket     = "tf-bucket"
    acl        = "private"
    
    tags = {
        Name = "Infra terraform"
    }
    
    versioning {
        enabled = true
    }
}