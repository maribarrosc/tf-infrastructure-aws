# module "transit_gateway" {
#   source         = "./modules/transit_gateway"
#   vpc_id         = "${module.vpc.vpc_id}"
#   public_subnet1 = "${module.vpc.subnet1}"
#   public_subnet2 = "${module.vpc.subnet2}"
# }

# clea

provider "aws" {
  region = "us-east-1"
}

# module "elb" {
#     source  = "./modules/elb"
#     name    = module.route53.name
#     # subnets = ""
#     vpc_id  = module.vpc.vpc_id
#     config  = locals.config
# }


# module "ec2" {
#   config                 = locals.config
#   source                 = "./modules/ec2"
#   name                   = "kibana"
#   ami                    = "ami-04b9e92b5572fa0d1"
#   instance_type          = "t2.small"
#   key_name               = "tf_ssh"
#   monitoring             = true
#   vpc_security_group_ids = ["sg-0184b06cf48f4d3f5"]
#   subnet_id              = "subnet-0cde0dbf3b324d50f"

#   tags = {
#     Terraform   = "true"
#     Environment = "dev"
#   }
# }

# module "route53" {
#     source            = "./modules/route53"
#     root_domain_name  = module.route53.name
#     config            = locals.config
# }

# module "postgresql_rds" {
#   source              = "./modules/databases"
#   vpc_id              = module.vpc.vpc_id
#   engine_version      = "11.5"
#   instance_class      = "db.t3.medium"
#   identifier          = "tf-postgres"
#   name                = "tf_pg"
#   username            = "show"
#   password            = "secret"
#   subnets             = []
#   subnets_cidrs       = []
#   encrypted           = false
#   config              = locals.config
# }

# module "certs" {
#   source                            = "./modules/certs"
#   domain_name                       = "tf.com.br"
#   subject_alternative_names         = ["*.tf.com.br"]
#   hosted_zone_id                    = aws_route53_zone.default.zone_id
#   validation_record_ttl             = "60"
#   allow_validation_record_overwrite = true
#   config                            = locals.config
# }

# module "elk" {
#   source  = "./modules/elk"
#   config = local.config
#   key = "tf_ssh"
#   private_key = "${file("~/.ssh/id_rsa")}"
# }